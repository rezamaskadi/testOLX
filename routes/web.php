<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->post('/', 'http://requestb.in/1cuwxyo1');
// $app->get('foo', function () {
//     return 'Hello World';
// });

// $app->get('user/{id}', function ($id) {
//     return 'User '.$id;	
// });

$app->post('api/phoneverify', 'UserController@saveUser');
$app->get('api/phoneverify', 'UserController@index');


// $app -> group(['prefix' => 'api/v1', 'namespace' => 
// 	'App\Http\Controllers'], function($app){
// 	$app->get('/home', 'YourController@method');	
// 	});