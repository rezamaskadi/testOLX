<?php
 
namespace App\Http\Controllers;
 
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
 
class UserController extends Controller{
 
 
    public function index(){
        
        $user  = User::all();
        
        return response()->json($user);
 
    }
 
    public function getUser($user_id){
 
        $user  = User::find($user_id);
 
        return response()->json($user);
    }
 
    public function saveUser(Request $request){
 
        $user = User::create($request->all());
 
        return response()->json($user);
 
    }
 
    public function deleteUser($user_id){
        $user  = User::find($user_id);
 
        $user->delete();
 
        return response()->json('success');
    }
 
    // public function updateUser(Request $request, $user_id){
    //     $user  = User::find($user_id);
 
    //     $user->phone_number = $request->input('title');
    //     $user-> = $request->input('content');
 
    //     $user->save();
 
    //     return response()->json($article);
    // }
 
}